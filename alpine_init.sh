setup-alpine -f ANSWERFILE

# setting fastest edge repos in repo list
grep "edge" /etc/apk/repositories | sed "s/^#//" > repositories
mv repositories /etc/apk/repositories

# setting the mirror
apk add curl
curl -LO http://nl.alpinelinux.org/alpine/MIRRORS.txt
repo_domain=`head -n 1 /etc/apk/repositories | sed -e "s/.*\/\///" | sed -e "s/\//\ /" | awk '{print $1}'`
mirror=`grep $repo_domain MIRRORS.txt`
rm MIRRORS.txt

# setting the chroot dir
mkdir -p /mnt/chroot
chroot_dir="/mnt/chroot"

# setting the arch (change if needed)
arch=$(uname -m)


# set up apk
version="2.12.7-r0"
curl -LO ${mirror}/latest-stable/main/${arch}/apk-tools-static-${version}.apk
tar -xzf apk-tools-static-*.apk
rm apk-tools-static-*.apk

# install the alpine base installation onto the chroot
./sbin/apk.static -X ${mirror}/latest-stable/main -U --allow-untrusted -p ${chroot_dir} --initdb add alpine-base

# seting up the chroot
mount -o bind /dev ${chroot_dir}/dev
mount -t proc none ${chroot_dir}/proc
mount -o bind /sys ${chroot_dir}/sys

# set up name resolution
cp -L /etc/resolv.conf ${chroot_dir}/etc/

# prepare the APK repositories
branch="edge" # change if you want a specific version
mkdir -p ${chroot_dir}/etc/apk
echo "${mirror}/${branch}/main" > ${chroot_dir}/etc/apk/repositories

cp chroot_init.sh ${chroot_dir}/chroot_init.sh
chroot ${chroot_dir} sh
